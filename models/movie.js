const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate');

const schema = Schema({
    title:String,
    genre: String,
    duration: Number,
    director: String,
    actors: [String]
});
schema.plugin(mongoosePaginate);
module.exports = mongoose.model('movies', schema);